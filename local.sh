#start venv
source .venv/bin/activate
#install requirements
pip install -r requirements.txt
#run app
python main.py