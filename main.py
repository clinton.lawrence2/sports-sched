import os
from kivy.app import App
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.spinner import Spinner
from kivy.clock import Clock
import nhlSched
import soccer
import traceback

class ButtonApp(App):
    def build(self):
        return super().build()

    def button_pressed(self, instance):
        self.show_processing_popup()
        Clock.schedule_once(lambda dt: self.process_request(instance), 0.1)

    def process_request(self, instance):
        try:
            numberMatches = 0
            if instance.text == "Phoenix Rising Home":
                numberMatches = soccer.getGames("Rising_Home")
            elif instance.text == "Phoenix Rising Away":
                numberMatches =  soccer.getGames("Rising_Away")
            elif instance.text == "USA Soccer(M)":
                numberMatches = soccer.getGames("USMNT")
            elif instance.text == "USA Soccer(W)":
                numberMatches = soccer.getGames("USWNT")
            elif instance.text == "NHL":
                numberMatches = nhlSched.getGames()
            elif instance.text == "Wake Up App":
                self.show_popup("Just Waking Up The App...")
            self.show_popup(f"{instance.text} Schedule retrieved successfully. Added {numberMatches} events")
        except Exception as e:
            self.show_popup(f"Failed to retrieve {instance.text} Schedule. Error: " + str(e))
            print(traceback.format_exc())

    def show_popup(self, message, *args):
        if hasattr(self, 'progress_popup'):
            self.progress_popup.dismiss()
        
        print(message)
        content = BoxLayout(orientation='vertical', padding=10)
        label = Label(text=message, size_hint_y=None)
        label.bind(texture_size=label.setter('size'))
        content.add_widget(label)
        
        popup = Popup(title='Results',
                      content=content,
                      size_hint=(0.8, 0.4))
        popup.open()

    def show_processing_popup(self):
        box = BoxLayout(orientation='vertical', padding=10)
        label = Label(text="Processing Request", size_hint_y=None)
        label.bind(texture_size=label.setter('size'))
        spinner = Spinner()
        box.add_widget(label)
        box.add_widget(spinner)
        
        self.progress_popup = Popup(title='Please Wait',
                                    content=box,
                                    size_hint=(0.8, 0.4),
                                    auto_dismiss=False)
        self.progress_popup.open()

if __name__ == "__main__":
    app = ButtonApp()
    app.run()
