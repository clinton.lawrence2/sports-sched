import logging
import calendar
import requests
from datetime import datetime, timedelta
from pytz import timezone
from googleCalendar import addEvent, get_calendar_service, createCal, getEvents

# logging config to stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())

def getTv(tvList: list) -> str:
    # Define the precedence order
    precedence = ["MAX", "ABC", "ESPN", "ESPN2","ESPN+"]
    
    tvNetwork = "ESPN+"
    # Loop through the broadcasts
    for broadcast in tvList:
        network = broadcast["network"]
        if network in precedence:
            if precedence.index(network) < precedence.index(tvNetwork):
                tvNetwork = network
        else:
            continue
    return tvNetwork

def getGames():
    baseURL = "https://api-web.nhle.com"
    resp = requests.get(url = baseURL+"/v1/schedule/now")
    randomData = resp.json()
    startDate = randomData["regularSeasonStartDate"]
    endDate = randomData["regularSeasonEndDate"]
    urlList = []

   
    scheduleURL = "/v1/schedule"

    date = startDate
    #if date isnt null keep looping
    while True:
        dateUrl = baseURL+scheduleURL+"/"+date
        urlList.append(dateUrl)
        resp = requests.get(url = dateUrl)
        randomData = resp.json()
        if "nextStartDate" in randomData.keys():
            date = randomData["nextStartDate"]
        else:
            break

    calService = get_calendar_service()

    existingEvents = []

    calId,calExists = createCal(calService,"NHL")
    if calExists:
        logger.info("Cal Already Existed,Get Events")
        existingEvents = getEvents(calService,calId)
        logger.info(f"Found {len(existingEvents)} Events")
        logger.debug(f"Events: {existingEvents}")

    numberMatches = 0
    for url in urlList:
        r = requests.get(url = url)
        games = r.json()
        gameWeeks = games["gameWeek"]
        for gameWeek in gameWeeks:
            for game in gameWeek["games"]:
                
                logger.debug(f"Adding Event {game['id']}")
                tvName= getTv(game["tvBroadcasts"])

                if "seriesStatus" in game.keys():
                    seriesData = game["seriesStatus"]
                    homeTeam = "{abv}({wins})".format(abv=seriesData["topSeedTeamAbbrev"],wins=seriesData["topSeedWins"])
                    awayTeam = "{abv}({wins})".format(abv=seriesData["bottomSeedTeamAbbrev"], wins=seriesData["bottomSeedWins"])
                    league = "Playoffs"
                    round = " - Game "+str(game["seriesStatus"]["gameNumberOfSeries"])
                else:
                    gameNumber = None
                    homeTeam = game["homeTeam"]["abbrev"]
                    awayTeam = game["awayTeam"]["abbrev"]
                    league = "Regular Season"
                    round = ""
                    if "specialEvent" in game.keys():
                        round = " - "+str(game["specialEvent"]["name"]["default"])
                  
                
                gameStartTime=game["startTimeUTC"]
                
                gameStartTime = datetime.strptime(game["startTimeUTC"], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=timezone('UTC'))
                gameEndTime = None
                
                allDay = False
                if gameStartTime == "Scheduled (Time TBD)":
                    gameEndTime = (gameStartTime + timedelta(days=1))
                    allDay = True
                else:
                    gameEndTime = (gameStartTime + timedelta(hours=3))
                
                
                gameIdTitle = "{away} vs {home}(TV:{tv})".format(away=awayTeam, home=homeTeam,tv=tvName)
                if "seriesStatus" in game.keys():
                    gameIdTitle = gameIdTitle.replace("(TV:", "(Game:{num}; TV:".format(num=gameNumber))
                
                gameIdDate  = gameStartTime.strftime("%m-%d-%Y")
                
                gameId = (gameIdDate + "_" + gameIdTitle).replace(" ","_")
                
                logger.debug(f"Game ID: {gameId}")

                if len(existingEvents) > 0:
                    if gameId in existingEvents:
                        logger.info(f"Event {gameId} Already Exists, Skipping")
                        continue

                finalGameData = {
                    "homeTeam":homeTeam,
                    "awayTeam":awayTeam,
                    "gameStartTime":gameStartTime,
                    "gameEndTime":gameEndTime,
                    "allDay":allDay,
                    "TV":tvName,
                    "gameNumber":gameNumber,
                    "league":league,
                    "round":round
                }

                logger.debug(f"Adding Event {gameId} to Calendar")
                addEvent(calService,calId,finalGameData)
                numberMatches += 1 
        
    return numberMatches    
