import requests
from datetime import datetime, timedelta
from googleCalendar import get_calendar_service, createCal, addEvent,getEvents
from pytz import timezone
import logging
import yaml
from googleapiclient.http import BatchHttpRequest

# logging config to stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())

def getGames(calKey):
    logger.debug("Getting Calendar Service....")
    calService = get_calendar_service()

    with open('soccerTeams.yaml', 'r') as file:
        teams = yaml.safe_load(file)["teams"]

    logger.debug("Teams Loaded")
    logger.debug(teams)

    details = teams[calKey]
 
    calName = details["calName"]

    calId,calExists = createCal(calService,calName)
    existingEvents = []
    if calExists:
        logger.info("Cal Already Existed, Retrieving previous events to avoid conflicts")
        existingEvents = getEvents(calService,calId)

    url = f"https://v3.football.api-sports.io/fixtures?season=2025&team={details['teamId']}&status=NS"

    headers = {
        'x-rapidapi-key': 'c36806391b64423190e53a40bf2f2fb0',
        'x-rapidapi-host': 'v3.football.api-sports.io'
    }

    response = requests.get(url, headers=headers)
    data = response.json()
    fixtures = data["response"]

    gamesData = {}
    for fixture in fixtures:
        homeTeam = fixture["teams"]["home"]["name"]
        awayTeam = fixture["teams"]["away"]["name"]
        league=fixture["league"]["name"]
        round=" - "+fixture["league"]["round"]
        
        if "Home" in calKey and homeTeam != "Phoenix Rising":
            continue
        elif "Away" in calKey and awayTeam != "Phoenix Rising":
            continue
        gameStartTime = datetime.strptime(fixture["fixture"]["date"], "%Y-%m-%dT%H:%M:%S%z").replace(tzinfo=timezone('UTC'))

        game = {
            "gameStartTime": gameStartTime,
            "gameEndTime": (gameStartTime + timedelta(hours=2)).replace(tzinfo=timezone('UTC')),
            "homeTeam": homeTeam,
            "awayTeam": awayTeam,
            "league" : league,
            "round" : round,
            "allDay": False
        }
        gameIdTitle = "{away} vs {home}".format(away=awayTeam, home=homeTeam)

        gameIdDate  = gameStartTime.strftime("%m-%d-%Y")
        gameId = (gameIdDate + "_" + gameIdTitle).replace(" ","_")
        gamesData[gameId] = game

    
    numberMatches = 0
    batch = calService.new_batch_http_request()
    logger.info("Adding Events to Calendar")   
    for gameId,gameData in gamesData.items():
        logger.debug(f"CHECKING IF {gameId} in {str(existingEvents)} {str(gameId in existingEvents)}")
        if gameId in existingEvents:
            logger.info("Event Already Exists, Skipping")
            continue
        batch.add(addEvent(calService, calId, gameData))
        numberMatches += 1

    batch.execute()
    return numberMatches